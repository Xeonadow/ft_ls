/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   statlist.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/06 13:46:13 by lpousse           #+#    #+#             */
/*   Updated: 2016/06/08 18:15:15 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
#include <stdio.h>

void		statlist_del(t_statlist **statlist)
{
	t_statlist	*tmp;

	while (*statlist != NULL)
	{
		tmp = (*statlist)->next;
		free((*statlist)->name);
		free((*statlist)->path);
		free((*statlist)->modes);
		if ((*statlist)->stat != NULL)
		{
			if (S_ISLNK(((*statlist)->stat)->st_mode))
				free((*statlist)->linkpath);
			free((*statlist)->stat);
			free((*statlist)->time);
			free(*statlist);
		}
		*statlist = tmp;
	}
}

t_statlist	*statlist_error(t_statlist *statlist, struct stat *statstruct)
{
	statlist->stat = NULL;
	statlist->modes = ft_strdup(strerror(errno));
	free(statstruct);
	return (statlist);
}

void		id_init(t_statlist *statlist, struct stat *statstruct)
{
	struct passwd	*uid;
	struct group	*gid;
	dev_t			dev;

	dev = statstruct->st_rdev;
	uid = getpwuid(statstruct->st_uid);
	statlist->owner = ft_strdup(uid->pw_name);
	gid = getgrgid(statstruct->st_gid);
	statlist->group = ft_strdup(gid->gr_name);
	statlist->major = major(dev);
	statlist->minor = minor(dev);
}

t_statlist	*statlist_new(char *path, struct stat *statstruct,
								int stat_ret, char *flags)
{
	t_statlist	*statlist;

	if (!(statlist = (t_statlist *)malloc(sizeof(t_statlist))))
		return (NULL);
	statlist->name = ft_strrchr(path, '/');
	if (statlist->name == NULL
		|| (flags[0] == 0 && !S_ISDIR(statstruct->st_mode)))
		statlist->name = ft_strdup(path);
	else
		statlist->name = ft_strdup((statlist->name) + 1);
	statlist->path = ft_strdup(path);
	if (statlist->name == NULL || statlist->path == NULL)
		return (NULL);
	if (stat_ret < 0)
		return (statlist_error(statlist, statstruct));
	statlist->stat = statstruct;
	statlist->modes = get_modes(statstruct);
	get_linkpath(statlist, path);
	id_init(statlist, statstruct);
	statlist->time = get_time(statstruct);
	statlist->nb_bytes = statstruct->st_size;
	statlist->next = NULL;
	return (statlist);
}

int			statlist_init(t_statlist **begin, char **pathlist,
										int nbpath, char *flags)
{
	struct stat	*statstruct;
	t_statlist	*new;
	int			i;
	int			stat_ret;

	*begin = NULL;
	i = -1;
	while (++i < nbpath)
	{
		if (!(statstruct = (struct stat *)malloc(sizeof(struct stat))))
			return (-1);
		stat_ret = lstat(pathlist[i], statstruct);
		if (stat_ret < 0 && pathlist[i][0] == '\0')
			return (-2);
		if (!(new = statlist_new(pathlist[i], statstruct, stat_ret, flags)))
			return (-1);
		if ((new->name)[0] == '.' && flags['a'] == 0 && flags[0] == 1)
			statlist_del(&new);
		sort(begin, new, flags);
	}
	if (flags[(int)'r'] == 1)
		revert(begin, flags);
	flags[0] = 1;
	return (0);
}
