/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/03 18:24:25 by lpousse           #+#    #+#             */
/*   Updated: 2016/06/09 16:27:34 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char		get_third_char(struct stat *statstruct, int n)
{
	if (n == 3)
	{
		if (S_ISUID & statstruct->st_mode)
			return (statstruct->st_mode & S_IXUSR ? 's' : 'S');
		else
			return (statstruct->st_mode & S_IXUSR ? 'x' : '-');
	}
	else if (n == 6)
	{
		if (S_ISGID & statstruct->st_mode)
			return (statstruct->st_mode & S_IXGRP ? 's' : 'S');
		else
			return (statstruct->st_mode & S_IXGRP ? 'x' : '-');
	}
	else if (S_ISVTX & statstruct->st_mode)
		return (statstruct->st_mode & S_IXOTH ? 't' : 'T');
	return (statstruct->st_mode & S_IXOTH ? 'x' : '-');
}

char		*get_modes(struct stat *stat)
{
	int			i;
	char		*str;
	static char	types[7] = "pcdb-ls";
	static int	macro[7] = {S_IFIFO, S_IFCHR, S_IFDIR, S_IFBLK,
							S_IFREG, S_IFLNK, S_IFSOCK};

	str = ft_strnew(10);
	if (!str)
		return (NULL);
	i = 0;
	while ((stat->st_mode & S_IFMT) != macro[i])
		i++;
	str[0] = types[i];
	str[1] = stat->st_mode & S_IRUSR ? 'r' : '-';
	str[2] = stat->st_mode & S_IWUSR ? 'w' : '-';
	str[3] = get_third_char(stat, 3);
	str[4] = stat->st_mode & S_IRGRP ? 'r' : '-';
	str[5] = stat->st_mode & S_IWGRP ? 'w' : '-';
	str[6] = get_third_char(stat, 6);
	str[7] = stat->st_mode & S_IROTH ? 'r' : '-';
	str[8] = stat->st_mode & S_IWOTH ? 'w' : '-';
	str[9] = get_third_char(stat, 9);
	return (str);
}

void		get_linkpath(t_statlist *statlist, char *path)
{
	struct stat	statstruct;
	char	buf[PATH_MAX];
	ssize_t	ret;

	statlist->linkisdir = 0;
	statlist->linkpath = NULL;
	if (!S_ISLNK((statlist->stat)->st_mode))
		return ;
	ret = readlink(path, buf, PATH_MAX);
	if (ret < 0)
		return ;
	statlist->linkpath = ft_strnew(ret);
	if (!(statlist->linkpath))
		return ;
	ft_strncpy(statlist->linkpath, buf, ret);
	if (stat(path, &statstruct) < 0)
		return ;
	statlist->linkisdir = S_ISDIR(statstruct.st_mode);
}

char		*get_time(struct stat *stat)
{
	char	*date;
	char	*ret;
	time_t	now;

	time(&now);
	date = ctime(&((stat->st_mtimespec).tv_sec));
	if (now < (stat->st_mtimespec).tv_sec
		|| now - ((stat->st_mtimespec).tv_sec) > 15552000)
	{
		ret = ft_strnew(12);
		ft_strncpy(ret, date + 4, 7);
		ft_strcat(ret, date + 19);
		return (ret);
	}
	ret = ft_strdup(date + 4);
	return (ret);
}

int			get_nb_bytes(t_statlist *statlist)
{
	if ((statlist->modes)[0] == 'c' || (statlist->modes)[0] == 'b')
		return (8);
	return (ft_intmaxlen_base(statlist->nb_bytes, 10));
}

t_fields	*get_fields(t_statlist *statlist, char *flags)
{
	t_fields	*f;
	t_fields	tmp;

	f = (t_fields *)malloc(sizeof(t_fields));
	f->nb_link = 0;
	f->owner = 0;
	f->group = 0;
	f->nb_bytes = 0;
	f->total = 0;
	while (statlist != NULL)
	{
		tmp.nb_link = ft_intlen((statlist->stat)->st_nlink);
		tmp.owner = ft_strlen(statlist->owner);
		tmp.group = ft_strlen(statlist->group);
		tmp.nb_bytes = get_nb_bytes(statlist);
		f->nb_link = tmp.nb_link > f->nb_link ? tmp.nb_link : f->nb_link;
		f->owner = tmp.owner > f->owner ? tmp.owner : f->owner;
		f->group = tmp.group > f->group ? tmp.group : f->group;
		f->nb_bytes = tmp.nb_bytes > f->nb_bytes ? tmp.nb_bytes : f->nb_bytes;
		f->total += flags['a'] == 1 || (statlist->name)[0] != '.' ?
			(statlist->stat)->st_blocks : 0;
		statlist = statlist->next;
	}
	return (f);
}
