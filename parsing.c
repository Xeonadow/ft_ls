/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/30 15:53:09 by lpousse           #+#    #+#             */
/*   Updated: 2016/06/09 17:21:39 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static int	flag_handle(char *arg, char *flags)
{
	int		j;
	
	j = 1;
	while (arg[j] != '\0')
	{
		if (ft_isoneof(arg[j], FLAGS) == 1)
			flags[(int)arg[j]] = 1;
		else
		{
			ft_dprintf("ft_ls: illegal option -- %c\n", arg[j]);
			ft_dprintf("usage: ls [-%s] [file ...]\n", FLAGS);
			return (-1);
		}
		j++;
	}
	return (0);
}

int			flag_parsing(int ac, char **av, char *flags)
{
	int		i;
	int		k;

	i = 1;
	k = 0;
	while (i < ac && av[i][0] == '-' && av[i][1] != '\0' && av[i][1] != '-')
	{
		if (flag_handle(av[i], flags) < 0)
			return (-1);
		i++;
	}
	if (i < ac && av[i][0] == '-' && av[i][1] == '-' && av[i][2] != '\0')
	{
		ft_dprintf("ft_ls: illegal option -- -\n");
		ft_dprintf("usage: ls [-%s] [file ...]\n", FLAGS);
		return (-1);
	}
	if (i < ac && ft_strequ("--", av[i]))
		k = 1;
	return (i + k);
}
