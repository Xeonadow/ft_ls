/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 14:57:06 by lpousse           #+#    #+#             */
/*   Updated: 2016/06/08 18:56:11 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void		put_spaces(int n)
{
	while (n > 0)
	{
		ft_putchar(' ');
		n--;
	}
}

void		ls_put_one(t_statlist *stat, char *flags, t_fields *fields)
{
	struct stat		*s;

	s = stat->stat;
	if (flags['l'] == 0)
		ft_printf("%s\n", stat->name);
	else
	{
		ft_printf("%s  ", stat->modes);
		put_spaces(fields->nb_link - ft_intlen(s->st_nlink));
		ft_printf("%u %s  ", s->st_nlink, stat->owner);
		put_spaces(fields->owner - ft_strlen(stat->owner));
		ft_printf("%s  ", stat->group);
		put_spaces(fields->group - ft_strlen(stat->group));
		
		if ((stat->modes)[0] == 'c' || (stat->modes)[0] == 'b')
		{
			put_spaces(fields->nb_bytes - 8);
			ft_printf("%3u, %3u ", stat->major, stat->minor);
		}
		else
		{
			put_spaces(fields->nb_bytes - ft_intmaxlen_base(stat->nb_bytes, 10));
			ft_printf("%lld ", stat->nb_bytes);
		}
		ft_printf("%.12s %s", stat->time, stat->name);
		if (stat->linkpath)
			ft_printf(" -> %s", stat->linkpath);
		ft_putchar('\n');
	}
}

void		ls_put(t_statlist *statlist, char *flags)
{
	t_fields	*fields;

	fields = NULL;
	if (flags['l'] == 1 && statlist != NULL)
	{
		fields = get_fields(statlist, flags);
		ft_printf("total %d\n", fields->total);
	}
	while (statlist != NULL)
	{
		ls_put_one(statlist, flags, fields);
		statlist = statlist->next;
	}
	if (flags['l'] == 1)
		free(fields);
}

int			ls_put0(t_statlist **statlist, char *flags)
{
	t_fields	*fields;

	fields = NULL;
	while (*statlist != NULL && (*statlist)->stat == NULL)
	{
		ft_dprintf("ft_ls: %s: %s\n", (*statlist)->path, (*statlist)->modes);
		*statlist = (*statlist)->next;
	}
	if (*statlist == NULL || ((*statlist)->modes)[0] == 'd'
		|| ((*statlist)->linkisdir && flags['l'] == 0))
		return (-1);
	if (flags['l'] == 1)
		fields = get_fields(*statlist, flags);
	while (*statlist != NULL && !(((*statlist)->modes)[0] == 'd')
				&& !((*statlist)->linkisdir && flags['l'] == 0))
	{
		ls_put_one(*statlist, flags, fields);
		*statlist = (*statlist)->next;
	}
	if (flags['l'] == 1)
		free(fields);
	return (0);
}
