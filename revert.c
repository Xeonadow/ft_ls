/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   revert.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/07 16:26:44 by lpousse           #+#    #+#             */
/*   Updated: 2016/06/07 16:27:49 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	revert1(t_statlist **statlist)
{
	t_statlist	*prev;
	t_statlist	*next;

	if (!(*statlist))
		return ;
	prev = NULL;
	while ((*statlist)->next != NULL)
	{
		next = (*statlist)->next;
		(*statlist)->next = prev;
		prev = (*statlist);
		*statlist = next;
	}
	(*statlist)->next = prev;
}

void	revert0(t_statlist **statlist)
{
	t_statlist	*prev;
	t_statlist	*dir;

	prev = NULL;
	dir = (*statlist);
	while (dir && !S_ISDIR((dir->stat)->st_mode))
	{
		prev = dir;
		dir = dir->next;
	}
	if (dir != NULL)
		revert1(&dir);
	if (prev != NULL)
	{
		prev->next = NULL;
		prev = (*statlist);
		revert1(statlist);
		prev->next = dir;
	}
	else if (dir != NULL)
		(*statlist) = dir;
}

void	revert(t_statlist **statlist, char *flags)
{
	t_statlist	*prev;
	t_statlist	*valid;

	if (flags[0] == 1)
		revert1(statlist);
	else
	{
		prev = NULL;
		valid = *statlist;
		while (valid && valid->stat == NULL)
		{
			prev = valid;
			valid = valid->next;
		}
		if (valid != NULL)
			revert0(&valid);
		if (prev != NULL)
			prev->next = valid;
		else
			(*statlist) = valid;
	}
}
