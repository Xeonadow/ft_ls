/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/13 18:59:14 by lpousse           #+#    #+#             */
/*   Updated: 2016/06/08 17:05:30 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# include <dirent.h>
# include <pwd.h>
# include <grp.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <errno.h>
# include <stdlib.h>
# include <unistd.h>
# include <limits.h>
# include <time.h>
# include "libft.h"

# define FLAGS "Ralrt1"

typedef struct	s_statlist
{
	char				*name;
	char				*path;
	struct stat			*stat;
	char				*modes;
	char				*linkpath;
	int					linkisdir;
	char				*time;
	char				*owner;
	char				*group;
	long long			nb_bytes;
	int					major;
	int					minor;
	struct s_statlist	*next;
}				t_statlist;

typedef struct	s_fields
{
	int						nb_link;
	int						owner;
	int						group;
	int						nb_bytes;
	unsigned long int		total;
}				t_fields;

int				flag_parsing(int ac, char **av, char *flags);
int				ft_ls(char **pathlist, int nbpath, char *flags, int rec);

char			**pathlist0(int i, int ac, char **av);
char			**pathlist_init(DIR *dir, int *nbpath, char *path);
char			**pathlist_rec(t_statlist *statlist, int *nbpath, char *flags);

int				statlist_init(t_statlist **begin, char **pathlist,
										int nbpath, char *flags);
void			statlist_del(t_statlist **statlist);

void			sort(t_statlist **statlist, t_statlist *new, char *flags);
void			revert(t_statlist **statlist, char *flags);

char			*get_modes(struct stat *stat);
void			get_linkpath(t_statlist *statlist, char *path);
char			*get_time(struct stat *stat);
t_fields		*get_fields(t_statlist *statlist, char *flags);

void			ls_put(t_statlist *stat, char *flags);
int				ls_put0(t_statlist **statlist, char *flags);

#endif
