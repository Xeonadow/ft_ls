/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/31 16:11:38 by lpousse           #+#    #+#             */
/*   Updated: 2016/06/07 18:44:59 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char	**pathlist_rec(t_statlist *statlist, int *nbpath, char *flags)
{
	char	**pathlist;
	int		i;

	if (!(pathlist = (char **)malloc(sizeof(char *))))
		return (NULL);
	pathlist[0] = NULL;
	i = 0;
	while (statlist != NULL)
	{
		if (S_ISDIR((statlist->stat)->st_mode)
			&& (flags[(int)'a'] == 1 || (statlist->name)[0] != '.')
			&& ft_strcmp(statlist->name, ".") != 0
			&& ft_strcmp(statlist->name, "..") != 0)
		{
			pathlist = ft_strtabadd(&pathlist, i, statlist->path);
			if (!pathlist)
				return (NULL);
			i++;
		}
		statlist = statlist->next;
	}
	*nbpath = i;
	return (pathlist);
}

char	*make_new_path(char const *s1, char const *s2)
{
	char	*str;
	int		len;

	if (s1[0] == '\0')
		return (ft_strdup(s2));
	len = ft_strlen(s1) + ft_strlen(s2) + 1;
	if ((str = ft_strnew(len)) == NULL)
		return (NULL);
	ft_strcpy(str, s1);
	ft_strcat(str, "/");
	ft_strcat(str, s2);
	return (str);
}

char	**pathlist_init(DIR *dir, int *nbpath, char *path)
{
	struct dirent	*dirent;
	char			**pathlist;
	char			**tmp;
	int				i;

	if (!(pathlist = (char **)malloc(sizeof(char *))))
		return (NULL);
	pathlist[0] = NULL;
	*nbpath = 0;
	while ((dirent = readdir(dir)) != NULL)
	{
		if (pathlist[0] != NULL)
		{
			tmp = pathlist;
			if (!(pathlist = (char **)malloc(sizeof(char *) * (*nbpath + 1))))
				return (NULL);
			i = -1;
			while (++i < *nbpath)
				pathlist[i] = tmp[i];
			free(tmp);
		}
		pathlist[*nbpath] = make_new_path(path, dirent->d_name);
		(*nbpath)++;
	}
	return (pathlist);
}

char	**pathlist0(int i, int ac, char **av)
{
	char	**pathlist;
	
	if (i == ac)
	{
		pathlist = (char **)malloc(sizeof(char **));
		pathlist[0] = ft_strdup(".");
	}
	else
		pathlist = av + i;
	return (pathlist);
}
