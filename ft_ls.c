/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/13 18:58:04 by lpousse           #+#    #+#             */
/*   Updated: 2016/06/07 18:58:10 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		ft_ls_dir(DIR *dir, char *flags, char *path)
{
	t_statlist		*begin;
	t_statlist		*statlist;
	char			**pathlist;
	int				nbpath;

	if (dir == NULL)
		return (-1);
	pathlist = pathlist_init(dir, &nbpath, path);
	if (closedir(dir) < 0 || !pathlist
		|| statlist_init(&begin, pathlist, nbpath, flags) < 0)
		return (-1);
	statlist = begin;
	ls_put(statlist, flags);
	ft_strtabdel(&pathlist, nbpath);
	if (!(pathlist = pathlist_rec(begin, &nbpath, flags)))
		return (-1);
	if (flags['R'] == 1)
		ft_ls(pathlist, nbpath, flags, 1);
	free(pathlist);
	statlist_del(&begin);
	return (0);
}

int		ft_ls(char **pathlist, int nbpath, char *flags, int rec)
{
	t_statlist	*statlist;
	t_statlist	*begin;
	int			ret;

	if ((ret = statlist_init(&begin, pathlist, nbpath, flags)) < 0)
		return (ret);
	statlist = begin;
	if (rec == 0)
		rec = ls_put0(&statlist, flags);
	while (statlist != NULL)
	{
		if ((begin != statlist || rec == 1) && rec >= 0)
			ft_putchar('\n');
		else
			rec = 0;
		if (rec == 1 || nbpath > 1)
			ft_printf("%s:\n", statlist->path);
		if (ft_ls_dir(opendir(statlist->path), flags, statlist->path) < 0)
			ft_dprintf("ft_ls: %s: %s\n", statlist->name, strerror(errno));
		statlist = statlist->next;
	}
	statlist_del(&begin);
	return (0);
}

int		main(int ac, char **av)
{
	char	flags[128];
	char	**pathlist;
	int		i;
	int		ret;

	i = -1;
	while (++i < 128)
		flags[i] = 0;
	i = flag_parsing(ac, av, flags);
	if (i < 1)
		return (-1);
	pathlist = pathlist0(i, ac, av);
	if ((ret = ft_ls(pathlist, (ac - i > 0 ? ac - i : 1), flags, 0)) < 0)
	{
		if (ret < -1)
			perror("ft_ls: open");
		else
			perror("ft_ls");
		return (-1);
	}
	return (0);
}
