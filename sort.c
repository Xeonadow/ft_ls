/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/25 17:19:47 by lpousse           #+#    #+#             */
/*   Updated: 2016/06/07 18:06:45 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		cmp_dir(t_statlist *n, t_statlist *s, char *flags)
{
	int				ndir;
	int				sdir;

	ndir = S_ISDIR((n->stat)->st_mode);
	sdir = S_ISDIR((s->stat)->st_mode);
	if (flags['k'] == 1)
		return (0);
	if (flags['l'] == 0)
	{
		if ((ndir || n->linkisdir) && !(sdir || s->linkisdir))
			return (1);
		if (!(ndir || n->linkisdir) && (sdir || s->linkisdir))
			return (-1);
	}
	else
	{
		if (ndir && !sdir)
			return (1);
		if (!ndir && sdir)
			return (-1);
	}
	return (0);
}

int		cmp(t_statlist *n, t_statlist *s, char *flags)
{
	struct timespec	*ntime;
	struct timespec	*stime;
	int				ret;

	if ((n->stat == NULL && s->stat) || (n->stat && s->stat == NULL))
		return (n->stat == NULL ? -1 : 1);
	if (n->stat == NULL && s->stat == NULL)
		return (ft_strcmp(n->path, s->path));
	if (flags[0] == 0 && (ret = cmp_dir(n, s, flags)) != 0)
		return (ret);
	ntime = (&(n->stat)->st_mtimespec);
	stime = (&(s->stat)->st_mtimespec);
	if (flags['t'] == 1)
	{
		if (ntime->tv_sec != stime->tv_sec)
			return (ntime->tv_sec > stime->tv_sec ? -1 : 1);
		if (ntime->tv_nsec != stime->tv_nsec)
			return (ntime->tv_nsec > stime->tv_nsec ? -1 : 1);
	}
	return (ft_strcmp(n->path, s->path));
}

void	sort(t_statlist **statlist, t_statlist *new, char *flags)
{
	t_statlist	*prev;
	t_statlist	*stat;

	if (new == NULL)
		return ;
	prev = NULL;
	stat = *statlist;
	while (stat != NULL && cmp(new, stat, flags) >= 0)
	{
		prev = stat;
		stat = stat->next;
	}
	if (prev == NULL)
	{
		new->next = *statlist;
		*statlist = new;
	}
	else
	{
		prev->next = new;
		if (stat != NULL)
			new->next = stat;
	}
}
